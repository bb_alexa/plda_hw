object SimpleLanguageRunner {
  def main(args: Array[String]): Unit = {
    val simpleParser = new SimpleParser
    val parsed = (simpleParser.parse(simpleParser.script, "def func(a){if(a==0){return 1}else{return a*func(a-1)}}\nfunc(a=4)"))

    //println("AST: "+parsed.get)

    val simpleInterpreter = new SimpleInterpreter(parsed.get)

    simpleInterpreter.run(parsed.get)
  }
}
