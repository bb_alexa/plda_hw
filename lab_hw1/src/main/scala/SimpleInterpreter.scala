import scala.collection.immutable.ListMap
class SimpleInterpreter(program: Seq[Expr]) {

  private var functionList: ListMap[String, List[Expr]] = new ListMap()
  private var functionArgsList: ListMap[String, Option[Args]] = new ListMap()
  private var variableList: ListMap[String, Any] = new ListMap()
  private var prev_result = 0


  // check the operator(+ - * /) and perform the operation
  private def handleBinaryOp(op: String, expr: Expr, expr1: Expr): Any = op match {
    case "+" => {
     if(expr.isInstanceOf[NumberVal] && expr1.isInstanceOf[NumberVal]){
       return expr.asInstanceOf[NumberVal].number.intValue() + expr1.asInstanceOf[NumberVal].number.intValue()
     }
      else{
       if(expr.isInstanceOf[Identifier] && expr1.isInstanceOf[NumberVal]){
         val id_val = handleIdentifier(expr.asInstanceOf[Identifier].value)
         variableList -= expr.asInstanceOf[Identifier].value
         variableList += (expr.asInstanceOf[Identifier].value -> (id_val.asInstanceOf[Int]-(expr1.asInstanceOf[NumberVal].number.intValue())))
         return id_val.asInstanceOf[Int]+(expr1.asInstanceOf[NumberVal].number.intValue())
       }
     }
  }
    case "-" => {
      if(expr.isInstanceOf[NumberVal] && expr1.isInstanceOf[NumberVal]){
        return expr.asInstanceOf[NumberVal].number.intValue() - expr1.asInstanceOf[NumberVal].number.intValue()
      }else {
        if(expr.isInstanceOf[Identifier] && expr1.isInstanceOf[NumberVal]){
          val id_val = handleIdentifier(expr.asInstanceOf[Identifier].value)
          variableList -= expr.asInstanceOf[Identifier].value
          variableList += (expr.asInstanceOf[Identifier].value -> (id_val.asInstanceOf[Int]-(expr1.asInstanceOf[NumberVal].number.intValue())))
          return id_val.asInstanceOf[Int]-(expr1.asInstanceOf[NumberVal].number.intValue())
        }
      }
  }
    case "/" => {
      if(expr.isInstanceOf[NumberVal] && expr1.isInstanceOf[NumberVal]){
        return expr.asInstanceOf[NumberVal].number.intValue() / expr1.asInstanceOf[NumberVal].number.intValue()
      }else{
        if(expr.isInstanceOf[Identifier] && expr1.isInstanceOf[NumberVal]){
          val id_val = handleIdentifier(expr.asInstanceOf[Identifier].value)
          variableList -= expr.asInstanceOf[Identifier].value
          variableList += (expr.asInstanceOf[Identifier].value -> (id_val.asInstanceOf[Int]-(expr1.asInstanceOf[NumberVal].number.intValue())))
          return id_val.asInstanceOf[Int]/(expr1.asInstanceOf[NumberVal].number.intValue())
        }
      }
  }
    case "*" => {
      if(expr.isInstanceOf[NumberVal] && expr1.isInstanceOf[NumberVal]){
        return expr.asInstanceOf[NumberVal].number.intValue() * expr1.asInstanceOf[NumberVal].number.intValue()
      }else{
        if(expr.isInstanceOf[Identifier] && expr1.isInstanceOf[Invoke]){
          val prev_id_value = prev_result
          val id_value = handleIdentifier(expr.asInstanceOf[Identifier].value)
          prev_result = id_value.asInstanceOf[Int]
          val temp = handleInvoke(expr1.asInstanceOf[Invoke].name,expr1.asInstanceOf[Invoke].args)
          return prev_id_value * id_value.asInstanceOf[Int]
        }else{
          if(expr.isInstanceOf[Identifier] && expr1.isInstanceOf[NumberVal]){
            val id_val = handleIdentifier(expr.asInstanceOf[Identifier].value)
            variableList -= expr.asInstanceOf[Identifier].value
            variableList += (expr.asInstanceOf[Identifier].value -> (id_val.asInstanceOf[Int]-(expr1.asInstanceOf[NumberVal].number.intValue())))
            return id_val.asInstanceOf[Int]*(expr1.asInstanceOf[NumberVal].number.intValue())
          }
        }
      }
    }
  }

  //check operator(== != ) and perform the operation
  private def handleCompare(op: String, expr: Expr, expr1: Expr): AnyVal ={
    op match {
      case "==" => {
        if(expr.isInstanceOf[NumberVal] && expr1.isInstanceOf[NumberVal]){
          if(expr.asInstanceOf[NumberVal].number.intValue() == expr1.asInstanceOf[NumberVal].number.intValue())
            return 1
          else
            return 0
        }
        else{
          if(expr.isInstanceOf[Identifier] && expr1.isInstanceOf[NumberVal] && variableList.contains(expr.asInstanceOf[Identifier].value)){
            if(variableList(expr.asInstanceOf[Identifier].value) == expr1.asInstanceOf[NumberVal].number.intValue())
              return 1
            else
              return 0
          }else{
            if(expr.isInstanceOf[Identifier] && expr1.isInstanceOf[Identifier] && variableList.contains(expr.asInstanceOf[Identifier].value) && variableList.contains(expr1.asInstanceOf[Identifier].value)){
              if(variableList(expr.asInstanceOf[Identifier].value) == variableList(expr1.asInstanceOf[Identifier].value))
                return 1
              else
                return 0
            }else {
              if(expr.isInstanceOf[NumberVal] && expr1.isInstanceOf[Identifier] && variableList.contains(expr1.asInstanceOf[Identifier].value)){
                if(expr.asInstanceOf[NumberVal].number.intValue() == variableList(expr1.asInstanceOf[Identifier].value))
                  return 1
                else
                  return 0
              }
            }
          }
        }
      }case "!=" => {
        if(expr.isInstanceOf[NumberVal] && expr1.isInstanceOf[NumberVal]){
          if(expr.asInstanceOf[NumberVal].number.intValue() != expr1.asInstanceOf[NumberVal].number.intValue())
            return 1
          else
            return 0
        }
        else{
          if(expr.isInstanceOf[Identifier] && expr1.isInstanceOf[NumberVal] && variableList.contains(expr.asInstanceOf[Identifier].value)){
            if(variableList(expr.asInstanceOf[Identifier].value) != expr1.asInstanceOf[NumberVal].number.intValue())
              return 1
            else
              return 0
          }else{
            if(expr.isInstanceOf[Identifier] && expr1.isInstanceOf[Identifier] && variableList.contains(expr.asInstanceOf[Identifier].value) && variableList.contains(expr1.asInstanceOf[Identifier].value)){
              if(variableList(expr.asInstanceOf[Identifier].value) != variableList(expr1.asInstanceOf[Identifier].value))
                return 1
              else
                return 0
            }else {
              if(expr.isInstanceOf[NumberVal] && expr1.isInstanceOf[Identifier] && variableList.contains(expr1.asInstanceOf[Identifier].value)){
                if(expr.asInstanceOf[NumberVal].number.intValue() != variableList(expr1.asInstanceOf[Identifier].value))
                  return 1
                else
                  return 0
              }
            }
          }
        }
      }
    }
  }

  //check if variable_id is in declared list, if not add to list variable name and value after executing the expression
  private def handleVarDecl(identifier: Identifier, expr: Expr) = {
    variableList += (identifier.value -> runExpr(expr))
  }

  // store name, args, expr list in function list
  private def handleFuncDef(identifier: String, maybeArgs: Option[Args], exprs: List[Expr]) = {
    if(!functionList.contains(identifier)){
      functionList += (identifier -> exprs)
      functionArgsList += (identifier -> maybeArgs)
      handleArgs(maybeArgs.head.expr, maybeArgs.head.nextOpt)
//      println("function list: "+functionList)
//      println("function args list: "+functionArgsList)
    }
    else
      println("WARN: function name already used")
  }

  //takes a list of expressions and executes the contained expressions one by one
  def runExprs(exps: List[Expr]): Any = {
    if (!exps.isEmpty){
      runExpr(exps.head)
      return runExprs(exps.tail)
    }
  }

  //takes an expression, evaluates it, and returns the result of the evaluation
  def runExpr(expr: Expr): Any = {
    expr match {
      case Invoke(name,args)=>{
        return handleInvoke(name,args)
    }
      case BinaryOP(op,expr1,expr2) =>{
        return handleBinaryOp(op,expr1,expr2)
    }
      case Compare(op,expr1,expr2) =>{
        return handleCompare(op,expr1,expr2)
    }
      case StringVal(value) =>{
        return value
    }
      case NumberVal(value) =>{
        return value.intValue()
    }
      case ReturnExpr(value) =>{
        return handleReturnExpr(value)
      }
      case VarDeclare(name,value) =>{
        return handleVarDecl(name,value)
      }
      case Identifier(name) =>{
        return handleIdentifier(name)
      }
      case Args(expr,nextOpt) =>{
        return handleArgs(expr,nextOpt)
      }
      case IfExpr(compare,exprs,exprs1) =>{
        return handleIf(compare,exprs,exprs1)
      }
    }
  }

  private def handleArgs(expr: Expr, maybeArgs: Option[Args]):Any ={
    if(expr != None && expr.isInstanceOf[Identifier] && !variableList.contains(expr.asInstanceOf[Identifier].value)) {
      variableList += (expr.asInstanceOf[Identifier].value -> 0)
      if (maybeArgs != None && maybeArgs.head.nextOpt == None) {
        handleArgs(maybeArgs.head.expr, None)
      }
    }
    else {
      if(expr.isInstanceOf[VarAss]){
        variableList += (expr.asInstanceOf[VarAss].identifier.value -> expr.asInstanceOf[VarAss].expr)
        handleVarAss(expr.asInstanceOf[VarAss].identifier,expr.asInstanceOf[VarAss].expr)
      }
      else{
        if(expr.isInstanceOf[BinaryOP]){
          handleBinaryOp(expr.asInstanceOf[BinaryOP].op,expr.asInstanceOf[BinaryOP].expr1, expr.asInstanceOf[BinaryOP].expr2)
        }
      }
    }
  }


  //search in declared variable list and return the value if name found
  private def handleIdentifier(str: String): Any = {
    if(variableList.contains(str))
      return variableList(str)
  }

  //takes the expression, evaluates it and prints out its result
  private def handleReturnExpr(expr: Expr): Any = {
    val result = runExpr(expr)
    println(result)
  }

  //search in defined functions list,and if function_id is found, execute function with the given arguments
  private def handleInvoke(identifier: Identifier, maybeArgs: Option[Args]):Any = {
    if(functionList.contains(identifier.value)) {
      //update args values in variable list
      handleArgs(maybeArgs.head.expr, maybeArgs.headOption)
      //execute statement list
      return runExprs(functionList(identifier.value))
    }
    else
      println("ERROR: function undefined!")
  }

  //stores the result of a function call in a variable
  private def handleFuncAss(identifier: Identifier, invoke: Invoke) = {
    if(variableList.contains(identifier.value)) {
      variableList -= identifier.value
      variableList += (identifier.value -> handleInvoke(invoke.name,invoke.args))
    }else
      println("ERROR: variable undefined!")
  }

  //If the variable is already defined, assigns the result of the expression to the variable with the given identifier
  private def handleVarAss(identifier: Identifier, expr: Expr) = {
    if(variableList.contains(identifier.value)) {
      variableList -= identifier.value
      variableList += (identifier.value -> runExpr(expr))
    }else
      println("ERROR: variable '"+identifier.value+"' undefined!")
  }

  private def handleIf(compare: Compare, exprs: List[Expr], exprs1: List[Expr]): Any = {
    if(runExpr(compare) == 1){
      runExprs(exprs)
    }
    else {
      runExprs(exprs1)
    }
  }
  //takes the AST provided by the parser and goes through the sequence of expressions
  def run(program: Seq[Expr]): Any = {
    if (!program.isEmpty) {
      program.head match {
        case FunctionDef(id, maybeArgs,instList) => {
          handleFuncDef(id.value,maybeArgs,instList)
          return run (program.tail)
        }
        case LambdaFuncDef(maybeArgs,instList) => {
          handleFuncDef("lambda",maybeArgs,instList)
          return run (program.tail)
        }
        case FunctionAss(id,invoke) => {
          handleFuncAss(id,invoke)
          return run (program.tail)
        }
        case Invoke(name,args) => {
          handleInvoke(name,args)
          return run (program.tail)
        }
        case VarDeclare(name,value) => {
          handleVarDecl(name,value)
          return run (program.tail)
        }
        case VarAss(identifier,p) => {
          handleVarAss(identifier,p)
          return run (program.tail)
        }
        case ReturnExpr(expr) => {
          handleReturnExpr(expr)
          return run (program.tail)
        }
        case BinaryOP(op, expr1 , expr2) => {
          handleBinaryOp(op,expr1,expr2)
          return run (program.tail)
        }
        case Compare(op, expr1, expr2) => {
          handleCompare(op,expr1,expr2)
          return run (program.tail)
        }
        case NumberVal(value) => {
          println(value)
          return run (program.tail)
        }
        case StringVal(value) => {
          println(value)
          return run (program.tail)
        }
      }
    }
  }
}
