import Type.{Fun, TypeVar}
import TypeInferencer.Answer
sealed trait Type

object Type {

  case class Fun(parameter: Type, result: Type) extends Type

  case object Number extends Type

  case object String extends Type

  case class TypeVar(id: Int) extends Type

}

object TypeInferencer {

  type Substitution = Map[TypeVar, Type]

  case class Answer(`type`: Type, substitution: Substitution)

  private def applyOneSub(type1: Type, typeVar: TypeVar, type2: Type): Type = type1 match {
    case Type.Number | Type.String => type1
    case Fun(parameter, result) => Fun(applyOneSub(parameter, typeVar, type2), applyOneSub(result, typeVar, type2))
    case TypeVar(_) => if (type1 == typeVar) type2 else type1
  }

  private def applySubs(typ: Type, subs: Substitution): Type = typ match {
    case Type.Number | Type.String => typ
    case Fun(parameter, result) => Fun(applySubs(parameter, subs), applySubs(result, subs))
    case typeVar@TypeVar(_) => subs.get(typeVar).getOrElse(typeVar)
  }

  private def extendSubs(subs: Substitution, typeVar: TypeVar, typ: Type): Substitution = {
    subs.map { case (lhs, rhs) => (lhs, applyOneSub(rhs, typeVar, typ)) }.updated(typeVar, typ)
  }

  private def unify(type1: Type, type2: Type, subs: Substitution, expr: Expr): Substitution = {
    val subed1 = applySubs(type1, subs)
    val subed2 = applySubs(type2, subs)
    (subed1, subed2) match {
      case (_, _) if (subed1 == subed2) => subs
      case (type1: TypeVar, _) => extendSubs(subs, type1, subed2)
      case (_, type2: TypeVar) => extendSubs(subs, type2, subed1)
      case (type1: Fun, type2: Fun) =>
        val unifiedSubs = unify(type1.parameter, type2.parameter, subs, expr)
        unify(type1.result, type2.result, unifiedSubs, expr)
      case _ => throw new Error(s"Type error: Unable to unify $subed1 and $subed2")
    }
  }

  def infer(expr: Expr): Type = {
    val freshTypeVar = (() => {
      // Hidden side effect! YAY!
      var counter = 0
      () => {
        val res = counter
        counter += 1
        TypeVar(res)
      }
    }) ()

    def inferBinOp(binOp: BinaryOP, typeEnv: Map[Expr, Type], subs: Substitution): Answer = {
      val Answer(leftType, leftSubs) = inner(binOp.expr1, typeEnv, subs)
      binOp.op match {
        case "+" | "-" | "*" | "/" =>
          val unifiedLeftSubs = unify(leftType, Type.Number, leftSubs, binOp.expr1)
          val Answer(rightType, rightSubs) = inner(binOp.expr2, typeEnv, unifiedLeftSubs)
          val unifiedRightSubs = unify(rightType, Type.Number, rightSubs, binOp.expr2)
          Answer(Type.Number, unifiedRightSubs)
      }
    }

    def inferCompare(cmp:Compare, typeEnv: Map[Expr, Type], subs: Substitution): Answer ={
      val Answer(leftType, leftSubs) = inner(cmp.expr1, typeEnv, subs)
      cmp.op match {
        case "==" | "!=" | "<=" | ">=" | "<" | ">" =>
          val unifiedLeftSubs = unify(leftType, Type.Number, leftSubs, cmp.expr1)
          val Answer(rightType, rightSubs) = inner(cmp.expr2, typeEnv, unifiedLeftSubs)
          val unifiedRightSubs = unify(rightType, Type.Number, rightSubs, cmp.expr2)
          Answer(Type.Number, unifiedRightSubs)
      }
    }

    def inner(expr: Expr, typeEnv: Map[Expr, Type], subs: Substitution): Any = expr match {
      case (expr: NumberVal) => Answer(Type.Number, subs)
      case (expr: StringVal) => Answer(Type.String, subs)
      case (expr: BinaryOP) => inferBinOp(expr, typeEnv, subs)
      case (expr: Compare) => inferCompare(expr,typeEnv,subs)
      case (expr: IfExpr) =>
        val Answer(condType, condSubs) = inner(expr.compare, typeEnv, subs)
        val unifiedCondSubs = unify(condType, Type.Number, condSubs, expr.compare)
        val Answer(thenType, thenSubs) = inner(expr.ifBranch.head, typeEnv, unifiedCondSubs)
        val Answer(elseType, elseSubs) = inner(expr.elseBranch.head, typeEnv, thenSubs)
        val unifiedSubs = unify(thenType, elseType, elseSubs, expr)
        Answer(elseType, unifiedSubs)
      case (expr: Identifier) =>
        if (typeEnv.get(expr) != None)
          Answer(typeEnv.get(expr).get, subs)
        else {
          println("TYPE ERROR: could not infer type!")
          System.exit(0)
        }
      case (expr: VarDeclare) =>
        inner(expr.value, typeEnv, subs)
      case (expr: VarAss) =>
        inner(expr.expr, typeEnv, subs)
      case (expr: FunctionDef) =>
        if (expr.maybeArgs != None) {
          if (expr.maybeArgs.head.nextOpt != None) {
            val arg1 = expr.maybeArgs.head.expr
            val argNext = expr.maybeArgs.head.nextOpt.head.expr
            val Answer(argType, _) = inner(expr.maybeArgs.head, typeEnv, subs)
            val Answer(argType2, _) = inner(expr.maybeArgs.head.nextOpt.head, typeEnv, subs)
            val Answer(resultType, resultSubs) = inner(expr.chains.head, typeEnv + (arg1.asInstanceOf[VarAss].identifier -> argType, argNext.asInstanceOf[VarAss].identifier -> argType2), subs)
            Answer(Type.Fun(argType, resultType), resultSubs)
          }else {
            val arg1 = expr.maybeArgs.head.expr
            val Answer(argType, _) = inner(expr.maybeArgs.head, typeEnv, subs)
            val Answer(resultType, resultSubs) = inner(expr.chains.head, typeEnv + (arg1.asInstanceOf[VarAss].identifier -> argType), subs)
            Answer(Type.Fun(argType, resultType), resultSubs)
          }
        }else {
          val Answer(resultType, resultSubs) = inner(expr.chains.head, typeEnv, subs)
          Answer(Type.Fun(resultType, resultType), resultSubs)
        }
      case (expr: Invoke) =>
        if (expr.args != None) {
          val Answer(argType, _) = inner(expr.args.head,typeEnv,subs)
          val arg = expr.args.head.expr
          Answer(Type.Fun(argType, argType), subs)
        }else {
          println("TYPE ERROR: could not infer type; too little info")
          System.exit(0)
        }
      case (expr: ReturnExpr) =>
        inner(expr.expr,typeEnv,subs)
      case (expr:Args) =>
        if(expr != None) {
          inner(expr.expr,typeEnv,subs)
        }
    }

    val Answer(typ, subs) = inner(expr, Map(), Map())
    applySubs(typ, subs)
  }
}
