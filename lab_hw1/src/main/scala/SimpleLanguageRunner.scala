object SimpleLanguageRunner {
  def main(args: Array[String]): Unit = {
    val simpleParser = new SimpleParser
    val parsed = simpleParser.parse(simpleParser.script, "111")
    val parsed1 = simpleParser.parse(simpleParser.script, "10+7")
    val parsed2 = simpleParser.parse(simpleParser.script, "if (1==1){12+3}else{11-1}")
    val parsed3 = simpleParser.parse(simpleParser.script, "var a = 44")
    val parsed4 = simpleParser.parse(simpleParser.script, "id = 2+1")
    val parsed5 = simpleParser.parse(simpleParser.script, "def func(a=1,b=1){return a+b}")
    val parsed6 = simpleParser.parse(simpleParser.script, "func(a=1,b=2)")

    println("AST: "+parsed.get.head)
    println("AST1: "+parsed1.get.head)
    println("AST2: "+parsed2.get.head)
    println("AST3: "+parsed3.get.head)
    println("AST4: "+parsed4.get.head)
    println("AST5: "+parsed5.get.head)
    println("AST6: "+parsed6.get.head)

    println("Type: "+TypeInferencer.infer(parsed.get.head)) // => Number
    println("Type1: "+TypeInferencer.infer(parsed1.get.head)) // => Number
    println("Type2: "+TypeInferencer.infer(parsed2.get.head)) // => Number
    println("Type3: "+TypeInferencer.infer(parsed3.get.head)) // => Number
    println("Type4: "+TypeInferencer.infer(parsed4.get.head)) // => Number
    println("Type5: "+TypeInferencer.infer(parsed5.get.head)) // => Number
    println("Type6: "+TypeInferencer.infer(parsed6.get.head)) // => Number

    //    val simpleInterpreter = new SimpleInterpreter(parsed.get)
//
//    simpleInterpreter.run(parsed.get)
  }
}
