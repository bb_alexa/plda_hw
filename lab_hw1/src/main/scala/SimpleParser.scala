import scala.util.parsing.combinator.RegexParsers

sealed trait Expr
sealed trait Val extends Expr{
  def eval(env:Map[String, Val]): Val = this
  def stringValue: String
}

case class NumberVal(number: Number) extends Val{
  def numberValue: Number = number
  override def stringValue: String = number.toString
  override def toString: String = number.toString
}
case class StringVal(str: String) extends Val{
  override def stringValue: String = str

  override def toString: String = str
}
case class NewLine() extends Expr

case class Variable(name:String) extends Expr {
  def eval(env:Map[String,Val]) = env(name)
  override def toString: String = name
}
case class Identifier(value: String) extends Expr
case class Args(expr: Expr, nextOpt: Option[Args]) extends Expr
case class BinaryOP(op: String, expr1:Expr, expr2: Expr) extends Expr
case class IfExpr(compare: Compare, ifBranch: List[Expr], elseBranch: List[Expr]) extends Expr
case class Compare(op: String, expr1: Expr, expr2: Expr) extends Expr
case class ReturnExpr(expr: Expr) extends Expr
case class VarAss(identifier: Identifier, expr: Expr) extends Expr
case class LambdaFuncDef(maybeArgs: Option[Args], chains: List[Expr]) extends Expr
case class FunctionDef(identifier: Identifier, maybeArgs: Option[Args], chains: List[Expr])extends Expr
case class FunctionAss(identifier: Identifier, invoke: Invoke) extends Expr
case class Invoke(name: Identifier, args: Option[Args]) extends Expr
case class VarDeclare(name: Identifier, value: Expr) extends Expr

class SimpleParser extends RegexParsers{

  def string: Parser[StringVal] = "\"[^\"]*\"".r ^^ { s => StringVal(s.substring(1, s.length - 1)) }
  def number: Parser[NumberVal] = "[0-9]+".r ^^ { n => NumberVal(n.toInt)}
  def newLine: Parser[NewLine]  = "[ ]*\n".r ^^ { _ => NewLine()}

  def expr: Parser[Expr] = (string | number | args | compare | returnExpr | ifExpr | invoke | varDeclare | varAss | functionDef | lambdaFuncDef | functionAss | binaryOp |compare) ^^ identity
  def identifier:Parser[Identifier] = "[a-zA-Z_]+".r ^^ Identifier.apply
  def args: Parser[Args] = (varAss|binaryOp|identifier) ~ ("," ~ (args)).? ^^ {
    case first ~ Some(_ ~ next) => Args(first, Some(next))
    case first ~ None => Args(first, None)
  }

  def ifExpr:Parser[IfExpr] = "if" ~ "(" ~ compare ~ ")" ~ "{" ~ (varDeclare|varAss|compare|binaryOp|returnExpr).+ ~ "}" ~ "else" ~ "{" ~ (varDeclare|varAss|compare|binaryOp|returnExpr).+ ~ "}" ^^ {
    case _ ~ _ ~ a ~_~ _ ~ b ~ _ ~ _ ~ _ ~ c ~ _ => IfExpr(a,b,c)
  }

  def lesserThan: Parser[Compare] = (identifier|number|string) ~ "<" ~ (identifier|number|string) ^^ {
    case a ~ b ~ c => Compare(b,a,c)
  }
  def greaterThan: Parser[Compare] = (identifier|number|string) ~ ">" ~ (identifier|number|string) ^^ {
    case a ~ b ~ c => Compare(b,a,c)
  }
  def equ: Parser[Compare] = (identifier|number|string) ~ "==" ~ (identifier|number|string) ^^ {
    case a ~ b ~ c => Compare(b,a,c)
  }
  def notEq: Parser[Compare] = (identifier|number|string) ~ "!=" ~ (identifier|number|string) ^^ {
    case a ~ b ~ c => Compare(b,a,c)
  }
  def lessEq: Parser[Compare] = (identifier|number|string) ~ "<=" ~ (identifier|number|string) ^^ {
    case a ~ b ~ c => Compare(b,a,c)
  }
  def greatEq: Parser[Compare] = (identifier|number|string) ~  ">=" ~ (identifier|number|string) ^^ {
    case a ~ b ~ c => Compare(b,a,c)
  }
  def compare: Parser[Compare] = (lesserThan|greaterThan|equ|notEq|lessEq|greatEq) ^^ identity

  def add: Parser[BinaryOP] = (invoke|identifier|number|string) ~ "+" ~ (invoke|identifier|number|string) ^^ {
    case a ~ op ~ b => BinaryOP(op,a,b)
  }
  def sub: Parser[BinaryOP] = (invoke|identifier|number) ~ "-" ~ (invoke|identifier|number) ^^ {
    case a ~ op ~ b => BinaryOP(op,a,b)
  }
  def mult: Parser[BinaryOP] = (invoke|identifier|number) ~ "*" ~ (invoke|identifier|number) ^^ {
    case a ~ op ~ b => BinaryOP(op,a,b)
  }
  def div: Parser[BinaryOP] = (invoke|identifier|number) ~ "/" ~ (invoke|identifier|number) ^^ {
    case a ~ op ~ b => BinaryOP(op,a,b)
  }
  def binaryOp: Parser[BinaryOP] = (add|sub|mult|div) ^^ identity

  def functionDef: Parser[FunctionDef] = "def" ~ identifier ~ "(" ~ args.? ~ ")" ~ "{" ~ (ifExpr|returnExpr|varDeclare|varAss).+ ~ "}" ^^ {
    case _ ~ id ~ _ ~ argsOpt ~ _ ~ _ ~ instrOpt ~ _ => FunctionDef(id, argsOpt,instrOpt)
  }

  def lambdaFuncDef: Parser[LambdaFuncDef] = "def" ~ "lambda" ~ "(" ~ args.? ~ ")" ~ "{" ~ (ifExpr|returnExpr|varDeclare|varAss).+ ~ "}" ^^ {
    case _ ~ _ ~ _ ~ argsOpt ~ _ ~ _ ~ instr ~ _ => LambdaFuncDef(argsOpt, instr)
  }

  def returnExpr: Parser[ReturnExpr] = "return" ~ (invoke|binaryOp|compare|identifier|string|number) ^^ {
    case _ ~ e => ReturnExpr(e)
  }
  def functionAss: Parser[FunctionAss] = identifier ~ "=" ~ invoke ^^ {
    case id ~ _ ~ func => FunctionAss(id, func)
  }

  def varAss:Parser[VarAss] = identifier ~  "=" ~ (binaryOp|number|string) ^^{
    case id ~ _ ~ op => VarAss(id,op)
  }

  def invoke: Parser[Invoke] = identifier ~ "(" ~ args.? ~ ")" ^^ {
    case func ~ _ ~ argsOpt ~ _ => Invoke(func, argsOpt)
  }
  def varDeclare: Parser[VarDeclare] = "var" ~ identifier ~ "=" ~ (invoke|binaryOp|compare|string|number) ^^ {
    case _ ~ name ~ _ ~ rhs => VarDeclare(name, rhs)
  }

  def instruction: Parser[Expr] = ( binaryOp | varDeclare | varAss |functionDef | lambdaFuncDef | invoke |  compare | returnExpr | ifExpr | number | string ) ^^ identity

  def script: Parser[Seq[Expr]] = {
    phrase(rep1(instruction | newLine)) ^^ { exprsOrNewLines => exprsOrNewLines.filterNot(_.isInstanceOf[NewLine]) }
  }
}
